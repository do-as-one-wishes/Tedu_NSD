#### 网络配置

卸载 NetworkManager，通过修改配置文件完整主机网络配置

```shell
[root@localhost ~]# systemctl stop NetworkManager
[root@localhost ~]# yum remove -y NetworkManager
[root@localhost ~]# systemctl enable --now network
[root@localhost ~]# vim /etc/sysconfig/network-scripts/ifcfg-eth0
# Generated by dracut initrd
DEVICE="eth0"
ONBOOT="yes"
NM_CONTROLLED="no"
TYPE="Ethernet"
BOOTPROTO="static"
IPADDR="192.168.1.20"
NETMASK="255.255.255.0"
GATEWAY="192.168.1.254"
[root@localhost ~]# reboot
```

修改模板

1、卸载 NetworkManager

2、卸载防火墙 yum -y remove firewalld-*

3、禁用 selinux，修改  /etc/selinux/config

​      SELINUX=disabled
