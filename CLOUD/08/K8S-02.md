# kubernetes -- 02

## kubernetes 基础管理

#### kubectl 命令

###### 命令说明

| 命令格式                                            | 命令说明                             |
| --------------------------------------------------- | ------------------------------------ |
| kubectl run 资源名称 -参数 --image=镜像名称:标签    | 创建资源对象，常用参数-i交互，-t终端 |
| kubectl get 查询资源  可选参数 -o wide 显示主机信息 | 常用查询的资源 node\|deployment\|pod |
| kubectl exec -it 容器id  执行的命令                 | 同 docker exec 指令，进入容器内      |
| kubectl describe 资源类型  资源名称                 | 查询资源的详细信息                   |
| kubectl attach                                      | 同 docker attach 指令，连接容器      |
| kubectl logs 容器id                                 | 查看容器控制台的标准输出             |
| kubectl delete 资源类型  资源名称                   | 删除指定的资源                       |
| kubectl create\|apply  -f 资源文件                  | 执行指定的资源文件                   |

###### 命令案例

```shell
# 执行指定的资源文件
[root@master flannel]# kubectl apply -f kube-flannel.yml

# get 查询信息
[root@master ~]# kubectl get nodes
NAME        STATUS   ROLES    AGE   VERSION
master      Ready    master   19h   v1.17.6
node-0001   Ready    <none>   16h   v1.17.6
[root@master ~]# kubectl -n kube-system get pod
NAME                             READY   STATUS    RESTARTS   AGE
kube-flannel-ds-amd64-hf2jp      1/1     Running   0          41m
kube-flannel-ds-amd64-rtl4l      1/1     Running   0          41m
... ...

# describe 查询详细信息
[root@master ~]# kubectl -n kube-system describe pod kube-flannel-ds-amd64-rtl4l
Name:                 kube-proxy-4tbp6
Namespace:            kube-system
... ...

# 使用run启动容器
[root@master ~]# kubectl run testos -it --image=192.168.1.100:5000/myos:v1804 
[root@testos-79778b4895-s8mxl /]# 
# 启动服务
[root@master ~]# kubectl run web-test --image=192.168.1.100:5000/myos:httpd
kubectl run --generator=deployment/apps.v1 is DEPRECATED and will be removed in a future version. Use kubectl run --generator=run-pod/v1 or kubectl create instead.
deployment.apps/web-test created
# 访问节点
[root@master ~]# kubectl get pod -o wide
NAME                        READY   STATUS    RESTARTS   AGE     IP
testos-79778b4895-s8mxl     1/1     Running   1          6m33s   10.244.3.2   ... ...
web-test-7bf98b9576-v566c   1/1     Running   0          4m24s   10.244.4.2   ... ...
[root@master ~]# curl http://10.244.4.2/info.php
<pre>
Array
(
    [REMOTE_ADDR] => 10.244.0.0
    [REQUEST_METHOD] => GET
    [HTTP_USER_AGENT] => curl/7.29.0
    [REQUEST_URI] => /info.php
)
php_host: 	web-test-7bf98b9576-v566c
1229

# 进入容器
[root@master ~]# kubectl exec -it testos-79778b4895-s8mxl -- /bin/bash
[root@testos-79778b4895-s8mxl /]# 
[root@master ~]# kubectl attach -it testos-79778b4895-s8mxl 
[root@testos-79778b4895-s8mxl /]#

# 查看终端日志
[root@master ~]# kubectl logs web-test-7bf98b9576-v566c 
AH00558: httpd: Could not reliably determine the server's fully qualified domain name, using 10.244.4.2. Set the 'ServerName' directive globally to suppress this message

# 删除资源，直接删除POD会自动重建
[root@master ~]# kubectl delete pod testos-79778b4895-s8mxl 
pod "testos-79778b4895-s8mxl" deleted
[root@master ~]# kubectl delete deployments testos
deployment.apps "testos" deleted
```

#### 资源文件

在k8s集群中，latest标签是不会被缓存，如果想缓存可以使用其他标签

```shell
[root@master ~]# docker pull 192.168.1.100:5000/myos:latest
[root@master ~]# docker tag 192.168.1.100:5000/myos:latest 192.168.1.100:5000/myos:v1804
[root@master ~]# docker push 192.168.1.100:5000/myos:v1804
[root@master ~]# curl http://192.168.1.100:5000/v2/myos/tags/list
{"name":"myos","tags":["nginx","php-fpm","latest","v1804","httpd"]}
```

###### pod-example资源文件

```yaml
[root@master config]# vim pod-example.yaml 
---
apiVersion: v1
kind: Pod
metadata:
  name: pod-example
  labels:
    app: myos
spec:
  containers:
  - name: myos
    image: 192.168.1.100:5000/myos:v1804
    stdin: true
    tty: true
  restartPolicy: Always
[root@master config]# kubectl apply -f pod-example.yaml 
pod/pod-example created
[root@master config]# kubectl get pod
NAME          READY   STATUS    RESTARTS   AGE
pod-example   1/1     Running   0          6s
[root@master config]# kubectl delete -f pod-example.yaml 
pod "pod-example" deleted
```

###### 简单web集群案例

```yaml
[root@master ~]# vim deployment-example.yaml 
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: httpd
spec:
  selector:
    matchLabels:
      app: myapp
  replicas: 3
  template:
    metadata:
      labels:
        app: myapp
    spec:
      containers:
      - name: apache
        image: 192.168.1.100:5000/myos:httpd
      restartPolicy: Always
[root@master ~]# kubectl apply -f deployment-example.yaml 
deployment.apps/httpd created
[root@master ~]# kubectl get pod
NAME                    READY   STATUS    RESTARTS   AGE
httpd-679f76478-8ltrw   1/1     Running   0          5s
httpd-679f76478-rcp76   1/1     Running   0          5s
httpd-679f76478-vwjvr   1/1     Running   0          5s
```

#### 集群调度

###### 扩容与缩减

```shell
[root@master ~]# kubectl get deployments.apps 
NAME    READY   UP-TO-DATE   AVAILABLE   AGE
httpd   3/3     3            3           60m
[root@master ~]# kubectl scale deployment httpd --replicas=2
deployment.apps/httpd scaled
[root@master ~]# kubectl get deployments.apps
NAME    READY   UP-TO-DATE   AVAILABLE   AGE
httpd   2/2     2            2           61m
```

###### 滚动更新集群

刚刚使用 deployment-example.yaml 创建的 apache，我们更新集群为 nginx

拷贝 deployment-example.yaml 进行编辑或使用云盘中的模板文件

```yaml
[root@master ~]# vim http-example-v2.yaml 
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: httpd
spec:
  selector:
    matchLabels:
      app: myapp
  replicas: 3
  revisionHistoryLimit: 10
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: myapp
    spec:
      containers:
      - name: apache
        image: 192.168.1.100:5000/myos:nginx
      restartPolicy: Always
[root@master ~]# kubectl apply -f http-example-v2.yaml 
# 查看历史版本并回滚
[root@master ~]# kubectl get deployments
NAME    READY   UP-TO-DATE   AVAILABLE   AGE
httpd   3/3     3            3           78m
[root@master ~]# kubectl rollout history deployment httpd 
deployment.apps/httpd 
REVISION  CHANGE-CAUSE
1         <none>
2         <none>
[root@master ~]# kubectl rollout undo deployment httpd --to-revision=1
```

###### daemonset控制器

```yaml
[root@master ~]# cat daemonset-expmple.yaml 
---
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: daemonset-example
  namespace: default
spec:
  selector:
    matchLabels:
      app: myapp1
  template:
    metadata:
      labels:
        app: myapp1
    spec:
      containers:
      - name: myos
        image: 192.168.1.100:5000/myos:v1804
        stdin: true
        tty: true
      restartPolicy: Always
[root@master ~]# kubectl apply -f daemonset-expmple.yaml 
```

###### 污点标签

```shell
[root@master ~]# kubectl get pod -o wide
NAME                    READY   STATUS    RESTARTS   AGE   IP           NODE     
httpd-679f76478-7x7rw   1/1     Running   0          72m   10.244.3.6   node-0003
httpd-679f76478-gk2rk   1/1     Running   0          72m   10.244.4.6   node-0002
httpd-679f76478-sqvvc   1/1     Running   0          72m   10.244.5.7   node-0001
[root@master ~]# kubectl taint node node-0003 k1=v1:NoExecute
node/node-0003 tainted
[root@master ~]# kubectl get pod -o wide
NAME                    READY   STATUS    RESTARTS   AGE   IP           NODE     
httpd-679f76478-gk2rk   1/1     Running   0          73m   10.244.4.6   node-0002
httpd-679f76478-qbkst   1/1     Running   0          6s    10.244.4.8   node-0002
httpd-679f76478-sqvvc   1/1     Running   0          73m   10.244.5.7   node-0001
# 查看污点标签
[root@master ~]# kubectl describe nodes node-0003 |grep -P "^Taints:"
Taints:             k1=v1:NoExecute
# 删除污点标签
[root@master ~]# kubectl taint node node-0003 k1-
```

###### 节点选择器

```yaml
[root@master ~]# vim http-example-v3.yaml 
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: httpd
spec:
  selector:
    matchLabels:
      app: myapp
  replicas: 1
  template:
    metadata:
      labels:
        app: myapp
    spec:
      nodeSelector:
        disktype: ssd
      containers:
      - name: apache
        image: 192.168.1.100:5000/myos:httpd
      restartPolicy: Always
[root@master ~]# kubectl apply -f http-example-v3.yaml 
deployment.apps/httpd created
[root@master ~]# kubectl get pod 
NAME                     READY   STATUS    RESTARTS   AGE
httpd-6bffbcb9d8-hgqvv   0/1     Pending   0          39s
[root@master ~]# kubectl label nodes node-0003 disktype=ssd
node/node-0003 labeled
[root@master ~]# kubectl get pod -o wide
NAME                     READY   STATUS    RESTARTS   AGE    IP           NODE
httpd-6bffbcb9d8-hgqvv   1/1     Running   0          2m8s   10.244.3.9   node-0003
```

###### job/cronjob控制器

```yaml
[root@master config]# vim job-example.yaml
---
apiVersion: batch/v1
kind: Job
metadata:
  name: pi
spec:
  template:
    spec:
      containers:
      - name: pi
        image: 192.168.1.100:5000/myos:v1804
        command: ["perl",  "-Mbignum=bpi", "-wle", "print bpi(500)"]
      restartPolicy: OnFailure
[root@master config]# kubectl apply -f job-example.yaml 
job.batch/pi created
[root@master config]# kubectl get job
NAME   COMPLETIONS   DURATION   AGE
pi     1/1           2s         7s
[root@master config]# kubectl get pod
NAME                     READY   STATUS      RESTARTS   AGE
httpd-6bffbcb9d8-hgqvv   1/1     Running     0          12m
pi-gvfwj                 0/1     Completed   0          15s
# 查看终端结果
[root@master config]# kubectl logs pi-gvfwj
```

```yaml
[root@master ~]# vim cronjob-example.yaml 
---
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: cronjob-pi
spec:
  schedule: "*/1 * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: pi
            image: 192.168.1.100:5000/myos:v1804
            command: ["perl",  "-Mbignum=bpi", "-wle", "print bpi(500)"]
          restartPolicy: OnFailure
[root@master ~]# kubectl apply -f cronjob-example.yaml 
cronjob.batch/cronjob-pi created
[root@master ~]# kubectl get cronjobs.batch 
NAME         SCHEDULE      SUSPEND   ACTIVE   LAST SCHEDULE   AGE
cronjob-pi   */1 * * * *   False     0        <none>          10s
[root@master ~]# kubectl get pod
NAME                          READY   STATUS      RESTARTS   AGE
cronjob-pi-1595410620-vvztx   0/1     Completed   0          62s
```
